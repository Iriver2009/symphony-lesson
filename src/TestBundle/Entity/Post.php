<?php

namespace TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constrains as Assert;

/** 
 * @ORM\Entity(repositoryClass="PostRepository")
 * @ORM\Table(name="posts")
 * @ORM\HasLifecycleCallbacks
 */
class Post
{
    /** 
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /** 
     * @ORM\Column(type="string", length=128)
     */
    protected $title;
    
    /** 
     * @ORM\Column(type="string", length=255)
     */
    protected $description;
    
    /** 
     * @ORM\Column(type="text", nullable=true)
     */
    protected $test;
    
    /** 
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $cteated;
    
    /** 
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updated;
    
    /** 
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $published = false;
    
    public function __toString()
    {
        return $this->title;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Post
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Post
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set test
     *
     * @param string $test
     * @return Post
     */
    public function setTest($test)
    {
        $this->test = $test;

        return $this;
    }

    /**
     * Get test
     *
     * @return string 
     */
    public function getTest()
    {
        return $this->test;
    }

    /**
     * Set published
     *
     * @param boolean $published
     * @return Post
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return boolean 
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Set cteated
     *
     * @param \DateTime $cteated
     * @return Post
     */
    public function setCteated($cteated)
    {
        $this->cteated = $cteated;

        return $this;
    }

    /**
     * Get cteated
     *
     * @return \DateTime 
     */
    public function getCteated()
    {
        return $this->cteated;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Post
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
